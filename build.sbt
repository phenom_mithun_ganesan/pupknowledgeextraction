name := "SparkGraphs"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.4"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.mongodb.spark" %% "mongo-spark-connector" % "2.4.1"
)
libraryDependencies += "org.scala-lang" % "scala-library" % "2.11.12"
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "2.4.4"
resolvers ++= Seq("Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven")

libraryDependencies += "graphframes" % "graphframes" % "0.7.0-spark2.4-s_2.11"
