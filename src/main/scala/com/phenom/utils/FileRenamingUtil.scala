package com.phenom.utils

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}


object FileRenamingUtil {
  private[utils] val hadoopConfig = new Configuration
  private[utils] val hdfs = FileSystem.get(hadoopConfig)

  def renameGeneratedCsvFile (path :String ,fileName : String,deleteSuccess :Boolean):Unit= {
    if(deleteSuccess) {
      val deletePath = new Path(path + "/_SUCCESS")
      hdfs.delete(deletePath, false)
    }
    val pathFiles = new Path(path)
    val fileNames = hdfs.listFiles(pathFiles, false)
    var fileNamesList = scala.collection.mutable.MutableList[String]()
    while (fileNames.hasNext) {
      fileNamesList += fileNames.next().getPath.getName
    }
    val partFileName = fileNamesList.filterNot(filenames => filenames == "_SUCCESS")
    val partFileSourcePath = new Path(path+"/"+ partFileName.mkString(""))
    val desiredCsvTargetPath = new Path(path +"/"+ fileName)
    hdfs.rename(partFileSourcePath , desiredCsvTargetPath)
  }
}
