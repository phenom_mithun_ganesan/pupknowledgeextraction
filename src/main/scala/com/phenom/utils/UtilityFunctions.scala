package com.phenom.utils

import java.util.Date

import org.bson.Document

object UtilityFunctions {
    def sortableDate(exp: Document): Date = {
    val startDate = exp.getDate("startDate")
    val endDate = exp.getDate("endDate")
    val flag = exp.getBoolean("isCurrent")
    if (startDate != null) return startDate
    if (endDate != null) return endDate
    if (flag != null && flag) return new Date
    null
  }


}
