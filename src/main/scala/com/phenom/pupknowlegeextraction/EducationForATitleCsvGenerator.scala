package com.phenom.pupknowlegeextraction

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{desc, explode,lower, udf}

class EducationForATitleCsvGenerator extends java.io.Serializable {

  def generateCsvFiles(dataFrame: DataFrame, sparkSession: SparkSession, writePath: String): Unit = {
  import sparkSession.implicits._
    dataFrame.show(10,false)
    val fdf = dataFrame.withColumn("jobTitle",explode($"experience.jobTitle")).withColumn("degree",explode($"education.degree"))
      .withColumn("fos",explode($"education.fieldOfStudy")).withColumn("jobTitle",lower($"jobTitle"))
      .withColumn("degree",lower($"degree")).withColumn("fos",lower($"fos"))
    fdf.filter(row => row.getAs("degree") != null && row.getAs("degree") != "" ).groupBy("jobTitle","degree").count().orderBy(desc("count")).show(10,false)
    fdf.filter(row => row.getAs("fos") != null && row.getAs("fos") != "" ).groupBy("jobTitle","fos").count().orderBy(desc("count")).show(100,false)


  }
}