package com.phenom.pupknowlegeextraction

import com.phenom.pupknowlegeextraction.TitleProgressionData.{expTitle, titleProgression}
import com.phenom.utils.{DateUtility, FileRenamingUtil}
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{desc, explode, lit, udf}

import scala.util.control.Breaks.{break, breakable}

class TitleProgressionCsvGenerator extends java.io.Serializable {
  case class expTitle(date :java.lang.Long,title :String,company:String)
  case class titleProgression(title1 :String,title2 :String,company:String)
  private def filterExp = udf((exp : Seq[Row])=>{
    var ret : Boolean = false
    if(!exp.isEmpty) ret = true
    breakable{
      for (e<-exp){
        if(e.getAs[java.lang.Long]("date") == null || e.getAs[String]("title").isEmpty ||e.getAs[String]("company").isEmpty){
          ret = false
          break()
        }
      }
    }
    ret
  })
  private def parseDates = udf((exp : Seq[Row])=>{
    var res : Seq[expTitle] = Seq()
    if(exp ==null)  res
    else {
      exp.foreach(a => {
        var temp =  DateUtility.parseDateToDate(a.getAs[String]("startDate"))
        var s1 : java.lang.Long= null
        if(temp!=null) {
          s1 = temp.getTime
        }
        if (s1 == null) {
          temp = DateUtility.parseDateToDate(a.getAs[String]("endDate"))
          if(temp!=null) s1 = temp.getTime
          if (s1 == null && a.getAs[Boolean]("isCurrent")) s1 = new java.util.Date().getTime
        }
        var title = a.getAs[String]("jobTitle")
        if(title == null) title =""
        else title = title.toLowerCase().trim
        var company = a.getAs[String]("company")
        if(company == null) company =""
        else company = company.toLowerCase().trim
        res = res:+expTitle(s1,title,company)
      })
      res
    }
  })
  private def convertToList = udf((exp : Seq[Row])=> {
    var res :Seq[String] = Seq()
    if(exp ==null) res
    else {
      for (i<- Range(0,exp.size) ){
        var tp = exp(i)
        res = res:+ tp.getAs[String]("title1")
        res = res:+ tp.getAs[String]("title2")
      }
      res
    }
  })

  private def addTitleProgression = udf((exp : Seq[Row])=>{
    var res : Seq[titleProgression] = Seq()
    if(exp ==null)  res
    else {
      var exp1 = exp.sortBy(_.getAs[java.lang.Long]("date"))
      var prevTitle = exp1(0).getAs[String]("title")
      var prevCompany = exp1(0).getAs[String]("company")
      for (i<- Range(1,exp.size) ){
        var currentTitle = exp1(i).getAs[String]("title")
        var currentCompany = exp1(i).getAs[String]("company")
        if(prevCompany.equals(currentCompany) && !prevTitle.equals(currentTitle)){
          res = res:+titleProgression(prevTitle,currentTitle,prevCompany)
        }
        prevCompany = currentCompany
        prevTitle = currentTitle
      }
    }
    res
  })
  def generateCsvFiles(dataFrame: DataFrame,sparkSession: SparkSession,writePath:String) :Unit={
    import sparkSession.implicits._
    val filtered_df = dataFrame.withColumn("parsedDates",parseDates($"experience")).select("parsedDates")
      .filter(filterExp($"parsedDates"))

    val progressionData = filtered_df.withColumn("progression",addTitleProgression($"parsedDates")).
      select("progression").withColumn("progression",explode($"progression"))
      .withColumn("name:START_ID",$"progression.title1")
      .withColumn("name:END_ID",$"progression.title2")
      .withColumn(":START_LABEL",lit("JobTitle"))
      .withColumn(":END_LABEL",lit("JobTitle"))
      .withColumn(":TYPE",lit("NEXT"))
      .drop("progression").groupBy("name:START_ID","name:END_ID"
    ,":START_LABEL",":END_LABEL",":TYPE").count()
      .withColumn("cnt:int",$"count").orderBy(desc("count")).drop("count")
      .coalesce(1)
     progressionData.write.option("header",true).csv(writePath+"/relationships")
   FileRenamingUtil.renameGeneratedCsvFile(writePath+"/relationships","relationships.csv",true)

      filtered_df.withColumn("progression",addTitleProgression($"parsedDates")).
      select("progression").withColumn("name:ID",convertToList($"progression"))
      .withColumn("name:ID",explode($"name:ID"))
      .select("name:ID").distinct().withColumn(":LABEL",lit("JobTitle")).coalesce(1)
      .write.option("header",true).csv(writePath+"/nodes")

    FileRenamingUtil.renameGeneratedCsvFile(writePath+"/nodes","nodes.csv",true)

  }
}
