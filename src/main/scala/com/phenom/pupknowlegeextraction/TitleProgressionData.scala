package com.phenom.pupknowlegeextraction
import org.apache.spark.sql.functions.explode
import org.apache.spark.sql.functions.desc
import org.apache.spark.sql.{Row, SparkSession}
import com.mongodb.spark._
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.SparkContext
import org.bson.Document
import org.apache.spark.sql.functions.udf
import scala.util.control.Breaks._
import com.phenom.utils.DateUtility
object TitleProgressionData
{
  case class expTitle(date :java.lang.Long,title :String,company:String)
  case class titleProgression(title1 :String,title2 :String,company:String)
  def main(args:Array[String])={
    val spark = SparkSession.builder().appName(s"${this.getClass.getSimpleName}").master("local[*]")
      .getOrCreate()
    import spark.implicits._
    val sqlContext = spark.sqlContext
    val readConfig = ReadConfig(Map("collection"->"PUPCandidatesList","database"->"ETG_KUNAGLOBAL",
      "uri"->"mongodb://MithunGanesan:FN4RtE56^RndSNN@qa-pupmongo01.aws.phenom.local:27017,qa-pupmongo02.aws.phenom.local:27017/"))
    val sc :  SparkContext = spark.sparkContext
    def filterExp = udf((exp : Seq[Row])=>{
      var ret : Boolean = false
      if(!exp.isEmpty) ret = true
      breakable{
        for (e<-exp){
           if(e.getAs[java.lang.Long]("date") == null || e.getAs[String]("title").isEmpty ||e.getAs[String]("company").isEmpty){
            ret = false
            break()
          }
        }
      }
      ret
    })
    def parseDates = udf((exp : Seq[Row])=>{
      var res : Seq[expTitle] = Seq()
      if(exp ==null)  res
      else {
        exp.foreach(a => {
          var temp =  DateUtility.parseDateToDate(a.getAs[String]("startDate"))
          var s1 : java.lang.Long= null
          if(temp!=null) {
            s1 = temp.getTime
          }
          if (s1 == null) {
            temp = DateUtility.parseDateToDate(a.getAs[String]("endDate"))
            if(temp!=null) s1 = temp.getTime
            if (s1 == null && a.getAs[Boolean]("isCurrent")) s1 = new java.util.Date().getTime
          }
          var title = a.getAs[String]("jobTitle")
          if(title == null) title =""
          else title = title.toLowerCase().trim
          var company = a.getAs[String]("company")
          if(company == null) company =""
          else company = company.toLowerCase().trim
          res = res:+expTitle(s1,title,company)
        })
        res
      }
    })
    def addTitleProgression = udf((exp : Seq[Row])=>{
      var res : Seq[titleProgression] = Seq()
      if(exp ==null)  res
      else {
        var exp1 = exp.sortBy(_.getAs[java.lang.Long]("date"))
        var prevTitle = exp1(0).getAs[String]("title")
        var prevCompany = exp1(0).getAs[String]("company")
        for (i<- Range(1,exp.size) ){
          var currentTitle = exp1(i).getAs[String]("title")
          var currentCompany = exp1(i).getAs[String]("company")
          if(/*prevCompany.equals(currentCompany) &&*/ !prevTitle.equals(currentTitle)){
            res = res:+titleProgression(prevTitle,currentTitle,prevCompany)
          }
          prevCompany = currentCompany
          prevTitle = currentTitle
        }
      }
      res
    })
    val mongoRdd = MongoSpark.load(sc, readConfig).withPipeline(Seq(/*Document.parse("{$limit:10000}"),*/
      Document.parse("{$project:{experience:1 , _id : 0}}")))
    val mongoDf = mongoRdd.toDF()
    //println("all documnets count:"+mongoDf.count)
    val parsedDf = mongoDf.withColumn("parsedDates",parseDates($"experience")).select("parsedDates")
    val filterdDf = parsedDf.filter(filterExp($"parsedDates"))
    val progressionDf = filterdDf.withColumn("progression",addTitleProgression($"parsedDates")).select("progression")
    //parsedDf.show(10,false)
  //  println("proogression df count:"+progressionDf.count())
    val explodeedDf = progressionDf.withColumn("progression",explode($"progression")).withColumn("title1",$"progression.title1")
        .withColumn("title2",$"progression.title2").withColumn("company",$"progression.company").drop("progression")
   // explodeedDf.show(10,false)
     val final_df = explodeedDf.groupBy("title1","title2").count().orderBy(desc("count"))
     final_df.coalesce(1).write.csv("/home/mithun/Documents/reports/inter_company.csv")
  }

}
