package com.phenom.pupknowlegeextraction

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.bson.Document

object App {
  def main(args:Array[String])= {
    val spark = SparkSession.builder().appName(s"${this.getClass.getSimpleName}").master("local[*]")
      .getOrCreate()
     val sqlContext = spark.sqlContext
     val readConfig = ReadConfig(Map("collection" -> "PUPCandidatesList", "database" -> "ETG_KUNAGLOBAL",
      "uri" -> "mongodb://MithunGanesan:FN4RtE56^RndSNN@qa-pupmongo01.aws.phenom.local:27017,qa-pupmongo02.aws.phenom.local:27017/"))
     val sc: SparkContext = spark.sparkContext
     val mongoRdd = MongoSpark.load(sc, readConfig).withPipeline(Seq(Document.parse("{$limit:1000}"),
      Document.parse("{$project:{experience:1, _id : 0}}")))
     val mongoDf = mongoRdd.toDF()
     val readPath = args(0)
     val writePath = args(1)
    // val pup_profiles = sqlContext.read.parquet(readPath)
     val titleProgressionCsvGenerator = new TitleProgressionCsvGenerator()
  //  val educationForATitleCsvGenerator = new EducationForATitleCsvGenerator()
   //  mongoDf.show(10,false)
       titleProgressionCsvGenerator.generateCsvFiles(mongoDf,spark,writePath)
     //   educationForATitleCsvGenerator.generateCsvFiles(mongoDf,spark,writePath)
  }
}
