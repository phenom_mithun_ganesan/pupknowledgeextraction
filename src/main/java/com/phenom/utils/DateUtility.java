package com.phenom.utils;

import org.apache.commons.lang.exception.ExceptionUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
public class DateUtility {
    private DateUtility(){}
    private static Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<>();

    static {
        DATE_FORMAT_REGEXPS.put("^\\d{8}$", "yyyyMMdd");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}-\\d{4}$", "MM-yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}$", "yyyy-MM");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}/\\d{4}$", "MM/yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{4}/\\d{1,2}$", "yyyy/MM");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
        DATE_FORMAT_REGEXPS.put("^[a-z]{3}\\s\\d{4}$", "MMM yyyy");
        DATE_FORMAT_REGEXPS.put("^[a-z]{4,}\\s\\d{4}$", "MMMM yyyy");
        DATE_FORMAT_REGEXPS.put("^\\d{12}$", "yyyyMMddHHmm");
        DATE_FORMAT_REGEXPS.put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
        DATE_FORMAT_REGEXPS.put("^\\d{14}$", "yyyyMMddHHmmss");
        DATE_FORMAT_REGEXPS.put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}\\.\\d{3}$", "dd-MM-yyyy HH:mm:ss.SSS");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2} z$", "yyyy-MM-dd HH:mm:ss z");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}\\.\\d{3}$", "yyyy-MM-dd HH:mm:ss.SSS");
        DATE_FORMAT_REGEXPS.put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}\\.\\d{3}\\s\\+\\d{4}$", "yyyy-MM-dd HH:mm:ss.SSS Z");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}\\.\\d{3}$", "dd MMMM yyyy HH:mm:ss.SSS");
        DATE_FORMAT_REGEXPS.put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}\\.\\d{3}\\+\\d{4}$", "dd MMMM yyyy HH:mm:ss.SSS Z");

    }

    /**
     * Determine SimpleDateFormat pattern matching with the given date string. Returns null if
     * format is unknown. You can simply extend DateUtil with more formats if needed.
     *
     * @param dateInString The date string to determine the SimpleDateFormat pattern for.
     * @return The matching SimpleDateFormat pattern, or null if format is unknown.
     * @see SimpleDateFormat
     */
    public static String parseDateToString(String dateInString) throws ParseException {
        if (dateInString == null || dateInString.trim().equals("")) {
            return dateToString(getDefaultDate());
        }
        Date date = null;
        List<SimpleDateFormat> formatterList = getFormatterList();
        for (SimpleDateFormat format : formatterList) {
            if (date != null) {
                break;
            }
            date = dateParser(dateInString, format);
        }
        if (date == null) date = checkInRegexDates(dateInString);
        if (date == null) date = getYearParser(dateInString);
        if (date == null) date = getDefaultDate();
        return dateToString(date);
    }

    public static List<SimpleDateFormat> getFormatterList() {
        List<SimpleDateFormat> formatterList = new ArrayList<>();
        formatterList.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"));
        formatterList.add(new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy"));
        formatterList.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
        formatterList.add(new SimpleDateFormat("yyyy-MM-dd"));
        return formatterList;
    }

    public static String dateToString(Date date) throws ParseException {
        Calendar cal = Calendar.getInstance();
        if (date == null) date = getDefaultDate();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        cal.set(1900 + date.getYear(), date.getMonth(), date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds());
        return formatter.format(cal.getTime());
    }

    public static Date dateParser(String date, SimpleDateFormat format) {
        if (date == null) return null;
        try {
            return format.parse(date);
        } catch (Exception e) {
            // LOGGER.error(e.getMessage());
        }
        return null;
    }

    public static Date checkInRegexDates(String dateInString) {
        if (dateInString == null) return null;
        dateInString = dateInString.toLowerCase().trim();
        Set<Map.Entry<String, String>> set = DATE_FORMAT_REGEXPS.entrySet();
        for (Map.Entry<String, String> item : set) {
            String regexp = item.getKey();
            if (dateInString.toLowerCase().matches(regexp)) {
                try {
                    return new SimpleDateFormat(DATE_FORMAT_REGEXPS.get(regexp)).parse(dateInString);
                } catch (Exception e) {
                    //LOGGER.error(e.getMessage());
                }
            }
        }
        return null;
    }

    public static Date getYearParser(String dateInString) {
        if (dateInString == null) return null;
        DateFormat formatter = new SimpleDateFormat("yyyy");
        String[] unknownDate = dateInString.split("[^0-9]");
        for (int i = 0; i < unknownDate.length; i++) {
            String date1 = unknownDate[i];
            if (date1.trim().length() == 4) {
                try {
                    return formatter.parse(date1);
                } catch (Exception e4) {
                    //LOGGER.error(e4.getMessage());
                }
            }
        }
        return null;
    }
    public static String checkNullString (Object object){
        if(object==null) return "";
        if(object.toString().trim().equals("null")) return "";
        return object.toString();
    }
    public static int calculateOverlap(Date s1,Date s2,Date e1,Date e2,
                                       String flag1,String flag2){
        int overlap = 0;
        if(s1==null || s2 ==null || (e1==null && !flag1.equalsIgnoreCase("true") )|| (e2 ==null
                && !flag2.equalsIgnoreCase("true"))) return overlap;
        long s1Time = s1.getTime();
        long s2Time = s2.getTime();
        long e1Time ;
        long e2Time ;
        if(flag1.equalsIgnoreCase("true") ){
            e1Time=System.currentTimeMillis();
        }else{
            e1Time =e1.getTime();
        }
        if(s1Time<0) s1Time = e1Time ;
        if(flag2.equalsIgnoreCase("true") ){
            e2Time=System.currentTimeMillis();
        }else{
            e2Time =e2.getTime();
        }
        if(s2Time<0) s2Time = e2Time;
        long start = Math.max(s1Time,s2Time);
        long end   = Math.min(e1Time,e2Time);
        if(end>start && start>0 && end > 0 ){
            overlap += convertMilliSecondsToDays(end-start)/30;
        }
        return overlap;
    }
    public static Long convertMilliSecondsToDays(Long time){
        if(time == null) return 0L;
        return (time/(1000*60*60*24));
    }
    public static Date getDefaultDate() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.parse("1111-01-01");
    }

    public static Date parseDateToDate(String dateInString) throws ParseException {
        if (dateInString == null || dateInString.trim().equals("")) {
            return null;
        }
        Date date = null;
        List<SimpleDateFormat> formatterList = getFormatterList();
        for (SimpleDateFormat format : formatterList) {
            if (date != null) {
                break;
            }
            date = dateParser(dateInString, format);
        }
        if (date == null) date = checkInRegexDates(dateInString);
        if (date == null) date = getYearParser(dateInString);
        return date;
    }
    public static String getPastDate(int n)  {
        n = n/12;
        Calendar cal = Calendar.getInstance();
        Date currentDate = cal.getTime();
        int month = currentDate.getMonth()+1;
        int year = currentDate.getYear();
        return ((1900-n+year)+"-"+month+"-01");
    }
    public static void sortDatesList(List<String> datesList){
        Collections.sort(datesList,(o1,o2)->{
            int i = -1;
            try {
                i = DateUtility.parseDateToDate(o1).compareTo(DateUtility.parseDateToDate(o2));
            } catch (ParseException e) {
                ExceptionUtils.getFullStackTrace(e);
            }
            return i;
        });
    }
   /* public static  void  main(String args[]) throws ParseException {
        org.bson.Document doc = new Document("userId","temp");
        Document doc2 = doc ;
        doc.append("this is lit","lool");
        System.out.println(doc);
        System.out.println(doc2);
    }*/
}
