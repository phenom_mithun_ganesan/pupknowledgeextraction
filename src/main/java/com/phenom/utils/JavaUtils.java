package com.phenom.utils;

import org.bson.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import com.phenom.utils.UtilityFunctions;

public class JavaUtils {
    public static List<Document> filterAndSort(ArrayList<Document> exp){
        List<Document> filteredList = exp.parallelStream().filter(a-> UtilityFunctions.sortableDate(a)!=null)
                .collect(Collectors.toList());
        if(filteredList.isEmpty() || filteredList.size()<exp.size()) new ArrayList<>();
        Collections.sort(filteredList,((a, b)->{
            return UtilityFunctions.sortableDate(a).compareTo(UtilityFunctions.sortableDate(b));
        }));
        return filteredList;
    }
}
